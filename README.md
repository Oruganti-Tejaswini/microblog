# Welcome to Wellfound's microblog assessment!

## Goal
Your task is to update the home page of this Flask app to display all of the rows in the blog posts table instead of just the static single row. Make this change on a separate branch (not ‘main’), and create a merge request. After you complete this, you’re done!

<BR><BR>

## Contributions
Feel free to clone this repo locally and use your own editor of choice.

If you would rather use the Gitpod development environment for this app:

- Change the dropdown that says "Web IDE" to "Gitpod" (if it already says "Gitpod" skip this step)
- Click the button that says "Gitpod"
